<h1>Mercedes-Benz Connected Vehicle Application</h1>
<p>This project written for connecting Mercedes-Benz vehicle and getting door status of this car.</p>
<br>
<h1>Docker Container Registry</h1>
<p>registry.gitlab.com/umitozturk/mb_connected_vehicle</p>
<p>please use port 8080</p>
<p><b>docker run -p 8080:8080 registry.gitlab.com/umitozturk/mb_connected_vehicle:latest</b></p>
<br>
<h1>Use this information for Spring Security</h1>
<p>admin</p>
<p>password</p>
<br>
<h1>You can also use this information for Mercedes-Benz API to login</h1>
<p>daimler.connectedvehicle@gmail.com</p>
<p>VehicleTest@@2019</p>
<br>
<h1>Built With</h1>
<p>Maven - Dependency Management</p>
<p>Spring Boot - Java-based framework</p>
<br>
<h1>Short video link</h1>
<p>[https://streamable.com/evz8r](https://streamable.com/evz8r)</p>
<br>
<h1>Personal Information</h1>
<p>[linkedin](https://www.linkedin.com/in/umitozturk34)</p>
<p>[github](https://github.com/umitozturk34)</p>
<p>ozturkumit34@gmail.com</p>