package cc.mc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: umitozturk
 * Custom Oauth2 configuration class
 *
 */
@Configuration
@EnableOAuth2Client
public class Oauth2Config {

    //Create OAuth2ProtectedResourceDetails for Mercedes api
    //TODO get rid of the hardcoded parts -> application.properties
    @Bean
    protected OAuth2ProtectedResourceDetails resource() {
        AuthorizationCodeResourceDetails  resource = new AuthorizationCodeResourceDetails();

        List<String> scopes = new ArrayList<>(2);
        scopes.add("mb:user:pool:reader");
        scopes.add("mb:vehicle:status:general");
        resource.setAccessTokenUri("https://api.secure.mercedes-benz.com/oidc10/auth/oauth/v2/token");
        resource.setClientId("a3e3af88-00d5-45b1-a131-d985ea9c2929");
        resource.setClientSecret("108b171a-8269-413d-ab9f-592be392eb66");
        resource.setGrantType("authorization_code");
        resource.setUserAuthorizationUri("https://api.secure.mercedes-benz.com/oidc10/auth/oauth/v2/authorize");
        resource.setScope(scopes);
        return resource;
    }

    /**
     * Get a new rest template with OAuth2 support for Mercedes
     * @param oauth2ClientContext The context that stores information for the oauth2 requests.
     * @return {@link OAuth2RestOperations} A new rest template that can be used to make requests to Mercedes Api
     */
    @Bean
    public OAuth2RestOperations restTemplate(OAuth2ClientContext oauth2ClientContext) {
        return new OAuth2RestTemplate(resource(), oauth2ClientContext);
    }

}
