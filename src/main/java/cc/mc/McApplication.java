package cc.mc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: umitozturk
 */
@SpringBootApplication
public class McApplication {

    public static void main(String[] args) {
        SpringApplication.run(McApplication.class, args);
    }

}
