package cc.mc.model;
public class Vehicle {

    private String id;
    private String licenseplate;
    private String finorvin;

    public String getId() { return this.id; }
    public void setId(String id) { this.id = id; }

    public String getLicenseplate() { return this.licenseplate;}
    public void setLicenseplate(String licenseplate) { this.licenseplate = licenseplate; }

    public String getFinorvin() { return this.finorvin;}
    public void setVFinorvin(String finorvin) { this.finorvin = finorvin; }


    @Override
    public String toString() {
        return "Vehicle{" +
                "id='" + id + '\'' +
                ", licenseplate='" + licenseplate + '\'' +
                ", finorvin='" + finorvin + '\'' +
                '}';
    }
}
