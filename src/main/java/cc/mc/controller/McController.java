package cc.mc.controller;

import cc.mc.model.Vehicle;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.MessageFormat;
import java.util.List;

/**
 * @Author: umitozturk
 */
@RestController
public class McController {

    private OAuth2RestOperations restTemplate;

    //constructor injection
    public McController(OAuth2RestOperations restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Test Controller
     *
     * @return {@link String} doors state
     */

    @RequestMapping(value = "/vehicle")
    public ModelAndView getVehicles() throws JSONException {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + restTemplate.getAccessToken());

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<List<Vehicle>> listOfVehicles =  restTemplate.exchange("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles", HttpMethod.GET, entity, new ParameterizedTypeReference<List<Vehicle>>() {
        });

        List<Vehicle> vehicleInfo = listOfVehicles.getBody();
        assert vehicleInfo != null;
        if(!vehicleInfo.isEmpty()) {
            String vehicleID = vehicleInfo.get(0).getId();
            String jsonString = restTemplate.exchange(MessageFormat.format("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/{0}/doors", vehicleID), HttpMethod.GET, entity, String.class).getBody();
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject lock = jsonObject.getJSONObject("doorlockstatusvehicle");
            String lockValue = lock.getString("value");
            ModelAndView mav = new ModelAndView("vehicle");
            mav.addObject("lock", lockValue);
            mav.addObject("id", vehicleID);
            return mav;
        }
        else {
            return  null;
        }
    }

    @GetMapping(value = "/lock/{id}")
    public ModelAndView lockDoors(@PathVariable("id") String id) {

        String requestJson = "{\"command\":\"LOCK\"}";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + restTemplate.getAccessToken());
        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);

        restTemplate.postForObject("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/"+ id +"/doors",  entity, String.class);

        ModelAndView mav = new ModelAndView("status");
        return mav;
    }

    @GetMapping(value = "/unlock/{id}")
    public ModelAndView unlockDoors(@PathVariable("id") String id) {

        String requestJson = "{\"command\":\"UNLOCK\"}";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + restTemplate.getAccessToken());
        HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);

        restTemplate.postForObject("https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/"+ id +"/doors",  entity, String.class);

        ModelAndView mav = new ModelAndView("status");
        return mav;
    }


}
