FROM openjdk:11
ADD target/mb_connected_vehicle.jar mb_connected_vehicle.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/mb_connected_vehicle.jar"]